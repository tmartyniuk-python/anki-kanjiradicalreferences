﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace KanjiRef
{
    enum ENote
    {
        Kanji,
        Vocab
    }

    internal class Program
    {
        const string sep = ", ";
        static Regex r = new Regex(">(.)<");


        public static void Main(string[] args)
        {
            var sb = new StringBuilder();
            const string filename = "c:/dev/scripts/Selected Notes.txt";
            var noteType = ENote.Vocab;
            var tag = noteType switch
            {
                ENote.Vocab => "vocab",
                ENote.Kanji => "kanji",
            };
            
            foreach (var line in File.ReadAllLines(filename, Encoding.UTF8))
            {
                var fields = line.Split('\t');
                if (fields[0].Contains("&nbsp;"))
                {
                    Console.WriteLine($"&nbsp; detected: {fields[0]}");
                }
                var tags = fields[fields.Length - 1];
                if (!tags.Contains(tag))
                    continue;

                string newLine;
                if (noteType == ENote.Vocab)
                {
                    newLine = AddRef(fields, 13,
                        k => $"<a onclick=\"pycmd('Browser search:Kanji:{k} tag:kanji')\">{k}</a>");
                }
                else
                {
                    newLine = AddRef(fields, 5, k => $"<a onclick=\"pycmd('Browser search:{k} tag:radical')\">{k}</a>");
                }

                sb.AppendLine(newLine);
            }

            File.WriteAllText(filename.Replace(".txt", $"_{tag}_out.tsv"), sb.ToString());
        }

        static string AddRef(string[] fields, int index, Func<string, string> tokenToRef)
        {
            var tokens = fields[index].Split(new[] { sep }, StringSplitOptions.None);
            var refs =
                string.Join(sep,
                    tokens.Select(k =>
                    {
                        if (k.Length > 1)
                        {
                            if (k.Length == 2 && k[0] == '\"')
                                k = k.Substring(1, 1);
                            else if (k.Contains("onclick"))
                                k = r.Match(k).Groups[1].Value;
                            else if (k.Contains(".png"))
                            {
                            }
                            else
                                Console.WriteLine($"strange token: {k} | {fields[1]} | {fields[fields.Length - 2]}");
                        }

                        return tokenToRef(k);
                    }).ToArray());

            fields[index] = refs;
            return string.Join("\t", fields);
        }
    }
}